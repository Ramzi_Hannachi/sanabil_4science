package com.sanabilmed.science4N;

import utils.SanabilActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;


public class Act_Home extends SanabilActivity {
	
	private static final int STOPSPLASH = 0;
	private static final long SPLASHTIME = 2000;
	private Handler splashHandler = new Handler() {
		
		public void handleMessage(Message msg) {

			Intent intent = new Intent(Act_Home.this, Act_Modules.class);
			Act_Home.this.startActivity(intent);
			Act_Home.this.finish();
			overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);

			super.handleMessage(msg);
		}
	};

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_home);

		Message msg = new Message();
		msg.what = STOPSPLASH;

		splashHandler.sendMessageDelayed(msg, SPLASHTIME);

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			splashHandler.removeMessages(STOPSPLASH);
		}
		return super.onKeyDown(keyCode, event);
	}


}