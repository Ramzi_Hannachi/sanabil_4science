package com.sanabilmed.science4N5p;

import android.os.Bundle;

import com.sanabilmed.science4N.R;

public class Act_Page3_m5 extends Act_Lecon5{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		imageDrawable = R.drawable.e4_m5_s3;
		sound = R.raw.e4_m5_s3;
		isZoomed = true;
		super.onCreate(savedInstanceState);
	}
	
}