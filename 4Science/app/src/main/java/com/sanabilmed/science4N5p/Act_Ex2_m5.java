package com.sanabilmed.science4N5p;

import java.util.Random;
import java.util.Vector;

import utils.Scalable;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.util.AttributeSet;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sanabilmed.science4N.R;






public class Act_Ex2_m5 extends Activity{

	private WakeLock mWakeLock;
	private ImageView rep, ex3,ex1, ex2, ex4, ex5, ex6, ex7, ex8, ex9, audio,
	imgVer, imgCorr, imgVerEx2, img1, img2, img3, img4, exit, home;
	private MediaPlayer mp;
	protected Vector<View> images;
	AnimationSet animation;
	
	private boolean scalingComplete = false;
	Activity act;
	ClipData data;
	int randomInt;

	private static final long active = 2000;
	private static final int STOP = 0;
	private Message msg;
	private ImageView prop_1_1,prop_1_2,prop_1_3,prop_1_4,prop_1_5,
	prop_2_1,prop_2_2 ;

	private RelativeLayout conteneur_1_1, conteneur_1_2, conteneur_1_3,
	conteneur_1_4, conteneur_1_5;


	//     _conteneur
	private Boolean _conteneur_1_1 = false, _conteneur_1_2 = false,
			_conteneur_1_3 = false, _conteneur_1_4 = false,
			_conteneur_1_5 = false;
	//     __conteneur
	private Boolean __conteneur_1_1 = false, __conteneur_1_2 = false,
			__conteneur_1_3 = false, __conteneur_1_4 = false,
			__conteneur_1_5 = false;
	int test = 0;
	private RelativeLayout zd_1, zd_2;
	int cont_1 = 0, cont_2 = 0,cont_3 = 0,cont_4 = 0,cont_5 = 0;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onCreate(Bundle savedInstanceState)
	{
		setContentView(R.layout.act_ex2_m5);
		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);
		img1 = (ImageView) findViewById(R.id.b1);

		act = this;


		//img2 = (ImageView) findViewById(R.id.b2);

		ex1 = (ImageView) findViewById(R.id.ex_1);
		ex3 = (ImageView) findViewById(R.id.ex_3);
		ex2 = (ImageView) findViewById(R.id.ex_2);
		
		imgCorr = (ImageView) findViewById(R.id.imgCorr);
		imgVer = (ImageView) findViewById(R.id.imgVer);
		rep = (ImageView) findViewById(R.id.repeat);
		audio = (ImageView) findViewById(R.id.jouerSon);

		// ////////////////////////////fin gestion
		// mp////////////////////////////////

		prop_1_1 = (ImageView) findViewById(R.id.obj1_1);
		prop_1_2 = (ImageView) findViewById(R.id.obj1_2);
		prop_1_3 = (ImageView) findViewById(R.id.obj1_3);
		prop_1_4= (ImageView) findViewById(R.id.obj1_4);
		prop_1_5 = (ImageView) findViewById(R.id.obj1_5);
		prop_2_1 = (ImageView) findViewById(R.id.obj2_1);
		prop_2_2 = (ImageView) findViewById(R.id.obj2_2);
		
		


		conteneur_1_1 = (RelativeLayout) findViewById(R.id.za1_1);
		conteneur_1_2 = (RelativeLayout) findViewById(R.id.za1_2);
		conteneur_1_3 = (RelativeLayout) findViewById(R.id.za1_3);
		conteneur_1_4 = (RelativeLayout) findViewById(R.id.za1_4);
		conteneur_1_5 = (RelativeLayout) findViewById(R.id.za1_5);
		



		zd_1 = (RelativeLayout) findViewById(R.id.zd1);
		zd_2 = (RelativeLayout) findViewById(R.id.zd2);
		


		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(2500);// 3500
		fadeOut.setDuration(1000);

		animation = new AnimationSet(true); // change to false
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);

		super.onCreate(savedInstanceState);

		prop_1_1.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) 
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					
					view.setVisibility(View.INVISIBLE);
					
					return true;
				}

				else 
				{
					
					prop_1_1.setVisibility(View.VISIBLE);
				
					return false;
				}
			}
		});
		
		prop_1_2.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) 
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					
					view.setVisibility(View.INVISIBLE);
					
					return true;
				}

				else 
				{
					
					prop_1_2.setVisibility(View.VISIBLE);
				
					return false;
				}
			}
		});
		prop_1_3.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_1_3.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_1_4.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_1_4.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_1_5.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_1_5.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		
		prop_2_1.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_2_1.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		
		prop_2_2.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent motionEvent) {

				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);

					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);

					return true;
				}

				else {
					prop_2_2.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		

		conteneur_1_1.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_1_1.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_1_1 || view4 == prop_1_2 || view4 == prop_1_3|| view4 == prop_1_4 || view4 == prop_1_5  ) 
					{
						_conteneur_1_1 = true;
						cont_1++;
					}
					test++;
					__conteneur_1_1 = true;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});

		conteneur_1_2.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					conteneur_1_2.setEnabled(false);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					if (view4 == prop_1_1 || view4 == prop_1_2 || view4 == prop_1_3|| view4 == prop_1_4 || view4 == prop_1_5 ) {
						_conteneur_1_2 = true;
						cont_2++;
					}
					test++;
					__conteneur_1_2 = true;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_1_3.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_1_3.setEnabled(false);
					if (view4 == prop_1_1 || view4 == prop_1_2 || view4 == prop_1_3|| view4 == prop_1_4 || view4 == prop_1_5  ) {
						_conteneur_1_3 = true;
						cont_3++;
					}
					__conteneur_1_3 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_1_4.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_1_4.setEnabled(false);
					if (view4 == prop_1_1 || view4 == prop_1_2 || view4 == prop_1_3|| view4 == prop_1_4 || view4 == prop_1_5  ) {
						_conteneur_1_4 = true;
						cont_4++;
					}
					__conteneur_1_4 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_1_5.setOnDragListener(new OnDragListener() {

			public boolean onDrag(View v, DragEvent event) {
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_1_5.setEnabled(false);
					if (view4 == prop_2_1  || view4 == prop_2_2) {
						_conteneur_1_5 = true;
						cont_5++;
					}
					__conteneur_1_5 = true;
					test++;

					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		
		
		imgVer.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				Random rand = new Random();
				randomInt = rand.nextInt();
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				try_stop_mp(mp);

				if (test < 5) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_incomplet);
					mp.start();
				} else if (cont_1 == 1 && cont_2 == 1 && cont_3 == 1 && cont_4 == 1 && cont_5 == 1) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_vrai_1);
					mp.start();

				} else {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);

					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_faux_2);
					mp.start();

					Message msg = new Message();
					msg.what = STOP;
					Handler.sendMessageDelayed(msg, active);
				}


				/*
				if (_rep1 == true) {
					rep1.setBackgroundResource(R.drawable.img1_f_m2_ex1);
				}

				if (_rep2 == true) {
					rep2.setBackgroundResource(R.drawable.img2_v_m2_ex1);
				}

				if (_rep3 == true) {
					rep3.setBackgroundResource(R.drawable.img3_f_m2_ex1);
				}

				if (_rep4 == true) {
					rep4.setBackgroundResource(R.drawable.img4_v_m2_ex1);
				}
				if (_rep5 == true) {
					rep5.setBackgroundResource(R.drawable.img5_v_m2_ex1);
				}
				if (_rep6 == true) {
					rep6.setBackgroundResource(R.drawable.img6_f_m2_ex1);
				}
				if (_rep7 == true) {
					rep7.setBackgroundResource(R.drawable.img7_v_m2_ex1);
				}
				if (_rep8 == true) {
					rep8.setBackgroundResource(R.drawable.img8_v_m2_ex1);
				}

				if ((_rep1 == false && _rep3 == false && _rep6 == false)
						&& (_rep2 == false || _rep4 == false || _rep5 == false
								|| _rep7 == false || _rep8 == false)) {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_incomplet);
					mp.start();

				}

				else if (_rep2 == true && _rep4 == true && _rep5 == true
						&& _rep7 == false && _rep8 == false) {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_vrai_1);
					mp.start();

					rep1.setEnabled(false);
					rep2.setEnabled(false);
					rep3.setEnabled(false);
					rep4.setEnabled(false);

				} else if (_rep1 == true && _rep3 == true && _rep4 == true
						&& _rep2 == true && _rep5 == true && _rep6 == true
						&& _rep7 == true && _rep8 == true) {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);

					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_faux_2);
					mp.start();
					mp.setOnCompletionListener(new OnCompletionListener() {

						@Override
						public void onCompletion(MediaPlayer mp) {
							_rep1 = false;
							_rep2 = false;
							_rep3 = false;
							_rep4 = false;
							_rep5 = false;
							_rep6 = false;
							_rep7 = false;
							_rep8 = false;
							rep1.setBackgroundResource(R.drawable.img1_m2_ex1);
							rep2.setBackgroundResource(R.drawable.img2_m2_ex1);
							rep3.setBackgroundResource(R.drawable.img3_m2_ex1);
							rep4.setBackgroundResource(R.drawable.img4_m2_ex1);
							rep5.setBackgroundResource(R.drawable.img5_m2_ex1);
							rep6.setBackgroundResource(R.drawable.img6_m2_ex1);
							rep7.setBackgroundResource(R.drawable.img7_m2_ex1);
							rep8.setBackgroundResource(R.drawable.img8_m2_ex1);
						}
					});
				} else {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);

					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_faux_2);
					mp.start();
					mp.setOnCompletionListener(new OnCompletionListener() {

						@Override
						public void onCompletion(MediaPlayer mp) {
							_rep1 = false;
							_rep3 = false;
							_rep6 = false;
							rep1.setBackgroundResource(R.drawable.img1_m2_ex1);
							rep3.setBackgroundResource(R.drawable.img3_m2_ex1);
							rep6.setBackgroundResource(R.drawable.img6_m2_ex1);
						}
					});
				}

				/*
				 * switch (verifierReponse()) { case 0:
				 * imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE); mp =
				 * MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_incomplet); mp.start(); break; case 1:
				 * imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE);
				 * 
				 * mp = MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_faux_2); mp.start(); break; case 2:
				 * imgCorr.setVisibility(1);
				 * imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
				 * imgCorr.startAnimation(animation);
				 * imgCorr.setVisibility(View.INVISIBLE);
				 * 
				 * mp = MediaPlayer.create(getApplicationContext(),
				 * R.raw.mascotte_vrai_1); mp.start(); break; }
				 */
			}
		});

//		img2.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				try_stop_mp(mp);
//				//Intent i2 = new Intent(getApplicationContext(), Page1_m3.class);
//			//	startActivity(i2);
//				act.finish();
//			}
//		});

		home.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
			//	startActivity(new Intent(getApplicationContext(), Act_Modules.class));

				act.finish();

			}
		});
		exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();

			}
		});
		img1.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v) 
			{
				try_stop_mp(mp);
				Intent i2 = new Intent(getApplicationContext(),Act_Page1_m5.class);
				startActivity(i2);
				act.finish();
			}
		});
		ex1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Ex1_m5.class));

				act.finish();

			}
		});

		ex3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Ex3_m5.class));

				act.finish();

			}
		});
	
				// Intent i2 = new Intent(getApplicationContext(),
				// Tri_1_Mod_1_Ex_6.class);
				// startActivity(i2);
				// act.finish();
			
		imgCorr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				imgCorr.setVisibility(View.INVISIBLE);

			}
		});




		rep.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();
				startActivity(getIntent());
			}
		});

		audio.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				mp = new MediaPlayer();
				mp = MediaPlayer.create(Act_Ex2_m5.this, R.raw.e4_m5_ex2_son);
				mp.start();
			}
		});

	}
	private Handler Handler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			if (!_conteneur_1_1 && __conteneur_1_1 == true) 
			{

				View v1 = conteneur_1_1.getChildAt(0);
				conteneur_1_1.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_1.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_1 = false;
			}
			if (!_conteneur_1_2 && __conteneur_1_2 == true) {

				View v1 = conteneur_1_2.getChildAt(0);
				conteneur_1_2.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_2.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_2 = false;
			}
			if (!_conteneur_1_3 && __conteneur_1_3 == true) {

				View v1 = conteneur_1_3.getChildAt(0);
				conteneur_1_3.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_3.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_3 = false;
			}
			if (!_conteneur_1_4 && __conteneur_1_4 == true) {

				View v1 = conteneur_1_4.getChildAt(0);
				conteneur_1_4.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_4.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_4 = false;
			}
			if (!_conteneur_1_5 && __conteneur_1_5 == true) {

				View v1 = conteneur_1_5.getChildAt(0);
				conteneur_1_5.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1_5.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1_5 = false;
			}
			

			super.handleMessage(msg);
		}

	};

	public void resetImage(View v) 
	{
		if (v == prop_1_1 || v == prop_1_2 || v == prop_1_3|| v == prop_1_4|| v == prop_1_5)
		{
			zd_1.addView(v);
			
		}
		
		if (v == prop_2_1 || v == prop_2_2) {
			zd_2.addView(v);
		}
		
	}
	public void try_stop_mp(MediaPlayer mp) {
		try {

			if (mp != null) {
				mp.stop();
				mp.release();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// protected void onDestroy() {
	// mp.release();
	// mWakeLock.release();
	// super.onDestroy();
	// }

	protected void onRestart() {
		super.onRestart();
	}

	protected void onPause() {
		super.onPause();
	}
	public boolean isOnline() {

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		Scalable.scaleContents(this);
	}
}