package com.sanabilmed.science4N5p;

import otherClasses.Hh;
import utils.SanabilActivity;
import utils.Utils;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.sanabilmed.science4N.R;

public class Act_Lecon5 extends SanabilActivity implements OnClickListener {

	private MediaPlayer mp, bg_mp;
	private ImageView imNuage, imBg, exit, home;
	private ImageView img2,img3,img4;
	protected boolean isZoomed;

	protected int imageDrawable, sound, bg_sound;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_m5_lecon5);

		Hh.StopedThread = false;
		mp = new MediaPlayer();
		bg_mp = new MediaPlayer();


		imNuage = (ImageView) findViewById(R.id.imNuage);
		imNuage.setOnClickListener(this);
		imNuage.setVisibility(View.INVISIBLE);
		imBg = (ImageView) findViewById(R.id.image_scene);

		imBg.setImageResource(imageDrawable);


		if (sound != 0) {
			mp = MediaPlayer.create(getApplicationContext(), sound);
			mp.start();
		}
		if (bg_sound != 0) {
			bg_mp = MediaPlayer.create(getApplicationContext(), bg_sound);
			bg_mp.start();
		}
		intNavBar();

		img2 = (ImageView) findViewById(R.id.b2);
//		img3 = (ImageView) findViewById(R.id.b_regles);
//		img4 = (ImageView) findViewById(R.id.b_prod);

		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);


		img2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Utils.stopMediaPlayer(mp);
				startActivity(new Intent(getBaseContext(), Act_Ex1_m5.class));
				finish();
			}
		});
//
//
//		img3.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				Utils.stopMediaPlayer(mp);
//				startActivity(new Intent(getBaseContext(), Act_Prod_Ex01.class));
//				finish();
//			}
//		});
//
//		img4.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				Utils.stopMediaPlayer(mp);
//				startActivity(new Intent(getBaseContext(), Act_Gram_Ex01.class));
//				finish();
//			}
//		});


		exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Utils.stopMediaPlayer(mp);
				Utils.stopMediaPlayer(bg_mp);
				finish();
			}
		});

		home.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Utils.stopMediaPlayer(mp);
				Utils.stopMediaPlayer(bg_mp);
				startActivity(new Intent(getBaseContext(), Module5.class));
				finish();
			}
		});
	}

	private void intNavBar() {

		ImageView im1=(ImageView) findViewById(R.id.BBar_Lecon1_image1);
		im1.setOnClickListener(this);
		ImageView im2=(ImageView) findViewById(R.id.BBar_Lecon1_image2);
		im2.setOnClickListener(this);
		ImageView im3=(ImageView) findViewById(R.id.BBar_Lecon1_image3);
		im3.setOnClickListener(this);
		ImageView im4=(ImageView) findViewById(R.id.BBar_Lecon1_image4);
		im4.setOnClickListener(this);
//		
		switch (Hh.selectedPage) {
		case 1:
			im1.setImageResource(R.drawable.ic_1);
			break;
		case 2:
			im2.setImageResource(R.drawable.ic_2);
			break;
		case 3:
			im3.setImageResource(R.drawable.ic_3);
			break;
		case 4:
			im4.setImageResource(R.drawable.ic_4);
			break;
		default:
			break;
		}
	}

	protected void onDestroy() {
		mp.release();
		bg_mp.release();
		super.onDestroy();
	}

	public void onClick(View arg0) {
		switch (arg0.getId()) {

		case R.id.imNuage:
			startActivity(new Intent(this, Act_Lecon5.class));
			Utils.animateFad(this);
			Utils.stopMediaPlayer(mp);
			Utils.stopMediaPlayer(bg_mp);
			finish();
			Hh.StopedThread = true;
			break;
		case R.id.BBar_Lecon1_image1:
			Hh.selectedPage=1;
			startActivity(new Intent(this,Act_Page1_m5.class));

			break;
		case R.id.BBar_Lecon1_image2:
			Hh.selectedPage=2;
			startActivity(new Intent(this,Act_Page2_m5.class));

			break;
		case R.id.BBar_Lecon1_image3:
			Hh.selectedPage=3;
			startActivity(new Intent(this,Act_Page3_m5.class));

			break;
		case R.id.BBar_Lecon1_image4:
			Hh.selectedPage=4;
			startActivity(new Intent(this,Act_Page4_m5.class));
			break;


		}
		Utils.animateFad(this);
		Utils.stopMediaPlayer(mp);
		Utils.stopMediaPlayer(bg_mp);
		finish();
	}

}
