package com.sanabilmed.science4N5p;

import java.util.Vector;

import android.app.Activity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class QuizDragAndDropWithImage4 extends Activity {

	protected Vector<ViewGroup> zone_depart;
	protected Vector<ViewGroup> zone_arrivee;
	protected Vector<View> images;
	protected Vector<View> rep_images;
	protected Vector<Image> image_id;
	boolean[] reponse;
	int[] reponse_pos;
	int nbr_question;
	private int x = 0;
	int i;
	private QuizDragAndDropWithImage4 act;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		nbr_question = zone_arrivee.size();
		reponse = new boolean[nbr_question];
		for (int j = 0; j < nbr_question; j++)
			reponse[i] = false;
		reponse_pos = new int[nbr_question];
		setImagesListener();
		zoneArriveeListeer();
		super.onCreate(savedInstanceState);
		act = this;
	}

	public void setImagesListener() {
		for (int i = 0; i < zone_depart.size(); i++) {
			images.get(i).setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View view, MotionEvent motionEvent) {
					if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
						DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
								view);
						view.startDrag(null, shadowBuilder, view, 0);
						view.setVisibility(View.VISIBLE);
						return true;
					} else {
						images.get(images.indexOf(view)).setVisibility(
								View.VISIBLE);
						return false;
					}
				}
			});
		}
	}

	public void zoneArriveeListeer() {
		for (int i = 0; i < nbr_question; i++) {
			zone_arrivee.get(i).setOnDragListener(new OnDragListener() {
				public boolean onDrag(View v, DragEvent event) {
					switch (event.getAction()) {
					case DragEvent.ACTION_DROP:
						View view = (View) event.getLocalState();
						if (((ViewGroup) v).getChildCount() != 0) {
							((ViewGroup) v).removeAllViews();
							reponse[zone_arrivee.indexOf((ViewGroup) v)] = false;
							// resetImage(v1);
							x--;
						}
						ImageView view2 = new ImageView(act);
						((ViewGroup) v).addView(view2);
						view2.setImageResource(image_id.get(images
								.indexOf(view)).rep_arrive);
						view2.setVisibility(View.VISIBLE);
						if (rep_images.get(zone_arrivee.indexOf(v)) == view) {
							reponse[zone_arrivee.indexOf(v)] = true;
						}
						reponse_pos[zone_arrivee.indexOf(v)] = images
								.indexOf(view);
						x++;
						break;
					default:
						break;
					}
					return true;
				}
			});
		}
	}

	// Verifier les reponse
	public int verifierReponse() {
		int nbr_rep_juste = 0;
		if (x < nbr_question)
			return 0;
		else {
			for (int i = 0; i < nbr_question; i++) {
				if (reponse[i] == true) {
					nbr_rep_juste++;
					((ImageView) zone_arrivee.get(i).getChildAt(0))
							.setImageResource(image_id.get(reponse_pos[i]).rep_vrai);
					((ViewGroup) zone_arrivee.get(i)).setEnabled(false);
					((ViewGroup) zone_arrivee.get(i)).getChildAt(0).setEnabled(false);
				} else {
					((ImageView) zone_arrivee.get(i).getChildAt(0))
							.setImageResource(image_id.get(reponse_pos[i]).rep_faux);
				}
			}
			if (nbr_rep_juste < nbr_question) {
				onReponsesFausses();
				return 1;
			} else {
				onReponsesJustes();
				return 2;
			}
		}
	}

	// Reset image
	public void resetImage(View v) {
		int pos = images.indexOf(v);
		zone_depart.get(pos).addView(v);
		((ImageView) v).setImageResource(image_id.get(pos).rep_defaut);
		v.setVisibility(View.VISIBLE);
	}

	// Remettre les fausses reponses a leurs places
	public void resetReponsesFausses() {
		for (int i = 0; i < nbr_question; i++) {
			if (!reponse[i]) {
				View v = ((ViewGroup) zone_arrivee.get(i)).getChildAt(0);
				((ViewGroup) zone_arrivee.get(i)).removeView(v);
				// resetImage(v);
				x--;
			}
		}
	}

	public void onReponsesJustes() {
		// TODO
	}

	public void onReponsesFausses() {
		// TODO
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

}
