package com.sanabilmed.science4N5p;

import android.os.Bundle;

import com.sanabilmed.science4N.R;

public class Act_Page4_m5 extends Act_Lecon5{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		imageDrawable = R.drawable.e4_m5_s4;
		sound = R.raw.e4_m5_s4;
		isZoomed = true;
		super.onCreate(savedInstanceState);
	}
	
}