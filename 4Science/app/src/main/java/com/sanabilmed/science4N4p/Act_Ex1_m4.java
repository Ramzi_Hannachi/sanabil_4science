package com.sanabilmed.science4N4p;

import java.util.Random;
import java.util.Vector;

import utils.Scalable;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sanabilmed.science4N.Act_Modules;
import com.sanabilmed.science4N.R;



public class Act_Ex1_m4 extends Activity
{
	private WakeLock mWakeLock;
	private ImageView rep, audio,
	imgVer, imgCorr, imgVerEx2, img1, img2, img3, img4, exit, home, suivant;
	private MediaPlayer mp;
	protected Vector<View> images;
	AnimationSet animation;

	private boolean scalingComplete = false;
	Activity act;
	ClipData data;
	int randomInt;

	private static final long active = 2000;
	private static final int STOP = 0;
	private Message msg;
	private ImageView prop_1_1, prop_1_2, prop_2_1, prop_2_2;

	private RelativeLayout conteneur_1, conteneur_2, conteneur_3,conteneur_4;
	// _conteneur
	private Boolean _conteneur_1 = false, _conteneur_2 = false,_conteneur_3 = false, _conteneur_4 = false;
	// __conteneur
	private Boolean  __conteneur_1 = false, __conteneur_2 = false, __conteneur_3 = false, __conteneur_4 = false;

	int test = 0;
	private RelativeLayout zd_1, zd_2;
	int cont_1 = 0;

	public void onCreate(Bundle savedInstanceState) 
	{
		setContentView(R.layout.act_ex1_m4);
		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);
		img1 = (ImageView) findViewById(R.id.b1);
		suivant=(ImageView) findViewById(R.id.suivant);

		act = this;

		// img2 = (ImageView) findViewById(R.id.b2);

		imgCorr = (ImageView) findViewById(R.id.imgCorr);
		imgVer = (ImageView) findViewById(R.id.imgVer);
		rep = (ImageView) findViewById(R.id.repeat);
		audio = (ImageView) findViewById(R.id.jouerSon);

		// ////////////////////////////fin gestion
		// mp////////////////////////////////

		prop_1_1 = (ImageView) findViewById(R.id.obj1_1);
		prop_1_2 = (ImageView) findViewById(R.id.obj1_2);
		prop_2_1 = (ImageView) findViewById(R.id.obj2_1);
		prop_2_2 = (ImageView) findViewById(R.id.obj2_2);

		conteneur_1 = (RelativeLayout) findViewById(R.id.za1);
		conteneur_2 = (RelativeLayout) findViewById(R.id.za2);
		conteneur_3 = (RelativeLayout) findViewById(R.id.za3);
		conteneur_4 = (RelativeLayout) findViewById(R.id.za4);

		zd_1 = (RelativeLayout) findViewById(R.id.zd1);
		zd_2 = (RelativeLayout) findViewById(R.id.zd2);

		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(2500);// 3500
		fadeOut.setDuration(1000);

		animation = new AnimationSet(true); // change to false
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);

		super.onCreate(savedInstanceState);

		prop_1_1.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent) 
			{
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_1_1.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});

		prop_1_2.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_1_2.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_2_1.setOnTouchListener(new OnTouchListener()
		{
			public boolean onTouch(View view, MotionEvent motionEvent) 
			{
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) 
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_2_1.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_2_2.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) 
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_2_2.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});

		conteneur_1.setOnDragListener(new OnDragListener() 
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) 
				{
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_1.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_2_1 || view4 == prop_2_2 )
					{
						_conteneur_1 = true;
						cont_1++;
					}
					test++;
					__conteneur_1 = true;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});

		conteneur_2.setOnDragListener(new OnDragListener() 
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					conteneur_2.setEnabled(false);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					if (view4 == prop_1_1 || view4 == prop_1_2 ) 
					{
						_conteneur_2 = true;
						cont_1++;
					}
					test++;
					__conteneur_2 = true;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_3.setOnDragListener(new OnDragListener() 
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_3.setEnabled(false);
					if (view4 == prop_1_1 || view4 == prop_1_2 )
					{
						_conteneur_3 = true;
						cont_1++;
					}
					__conteneur_3 = true;
					test++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});
		conteneur_4.setOnDragListener(new OnDragListener()
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) 
				{
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					view4.setEnabled(false);
					conteneur_4.setEnabled(false);
					if (view4 == prop_2_1 || view4 == prop_2_1 )
					{
						_conteneur_4 = true;
						cont_1++;
					}
					__conteneur_4 = true;
					test++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});


		imgVer.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				Random rand = new Random();
				randomInt = rand.nextInt();
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				try_stop_mp(mp);

				if (test < 4) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_incomplet);
					mp.start();
				} else if (cont_1 == 4) 
				{
					//ChangerColorImage_Vert();
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_vrai_1);
					mp.start();

				} else {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_faux_2);
					mp.start();

					Message msg = new Message();
					msg.what = STOP;
					Handler.sendMessageDelayed(msg, active);
				}

			}
		});

		// img2.setOnClickListener(new OnClickListener() {
		// public void onClick(View v) {
		// try_stop_mp(mp);
		// //Intent i2 = new Intent(getApplicationContext(), Page1_m3.class);
		// // startActivity(i2);
		// act.finish();
		// }
		// });

		home.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(),
						Act_Modules.class));

				act.finish();

			}
		});
		exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();

			}
		});


		// Intent i2 = new Intent(getApplicationContext(),
		// Tri_1_Mod_1_Ex_6.class);
		// startActivity(i2);
		// act.finish();

		imgCorr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				imgCorr.setVisibility(View.INVISIBLE);

			}
		});

		rep.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();
				startActivity(getIntent());
			}
		});

		audio.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) {
				try_stop_mp(mp);
				mp = new MediaPlayer();
				mp = MediaPlayer.create(Act_Ex1_m4.this, R.raw.e4_m10_ex1);
				mp.start();
			}
		});

		suivant.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				//				startActivity(new Intent(getApplicationContext(),
				//						Act_Modules.class));
				//
				//				act.finish();

			}
		});

	}





	private Handler Handler = new Handler() 
	{
		public void handleMessage(Message msg)
		{

			if (!_conteneur_1 && __conteneur_1 == true) 
			{
				View v1 = conteneur_1.getChildAt(0);
				conteneur_1.removeView(v1);
				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_1.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_1 = false;
			}

			if (!_conteneur_2 && __conteneur_2 == true)
			{
				View v1 = conteneur_2.getChildAt(0);
				conteneur_2.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_2.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_2 = false;
			}

			if (!_conteneur_3 && __conteneur_3 == true) 
			{
				View v1 = conteneur_3.getChildAt(0);
				conteneur_3.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_3.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_3 = false;
			}

			if (!_conteneur_4 && __conteneur_4 == true)
			{
				View v1 = conteneur_4.getChildAt(0);
				conteneur_4.removeView(v1);

				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;

				conteneur_4.setEnabled(true);
				v1.setEnabled(true);
				__conteneur_4 = false;
			}


			super.handleMessage(msg);
		}

	};

	public void resetImage(View v)
	{
		if (v == prop_1_1)
		{
			prop_1_1.setImageResource(R.drawable.s4_m10_ex1_oui_v);
			zd_1.addView(prop_1_1);
		}
		if (v == prop_1_2)
		{
			prop_1_2.setImageResource(R.drawable.s4_m10_ex1_oui_v);
			zd_1.addView(prop_1_2);
		}
		if (v == prop_2_1)
		{
			prop_2_1.setImageResource(R.drawable.s4_m10_ex1_non_v);
			zd_2.addView(prop_2_1);
		}
		if (v == prop_2_2)
		{
			prop_2_2.setImageResource(R.drawable.s4_m10_ex1_non_v);
			zd_2.addView(prop_2_2);
		}
	}





	public void try_stop_mp(MediaPlayer mp) {
		try {

			if (mp != null) {
				mp.stop();
				mp.release();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// protected void onDestroy() {
	// mp.release();
	// mWakeLock.release();
	// super.onDestroy();
	// }

	protected void onRestart() {
		super.onRestart();
	}

	protected void onPause() {
		super.onPause();
	}

	public boolean isOnline() {

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		Scalable.scaleContents(this);
	}
}