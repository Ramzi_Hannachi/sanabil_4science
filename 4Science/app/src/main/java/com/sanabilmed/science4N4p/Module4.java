package com.sanabilmed.science4N4p;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sanabilmed.science4N.Act_Modules;
import com.sanabilmed.science4N.R;

public class Module4 extends Activity implements OnClickListener 
{
	private ImageView mod1, mod2, mod3, mod4, exit, home;
	private boolean scalingComplete = false;	

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.module1_m4);

		mod1 = (ImageView) findViewById(R.id.mod1_1);
		mod1.setOnClickListener(this);
		mod2 = (ImageView) findViewById(R.id.mod1_2);
		mod2.setOnClickListener(this);
		exit = (ImageView) findViewById(R.id.exit);
		exit.setOnClickListener(this);
		home = (ImageView) findViewById(R.id.home);
		home.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId()) 
		{
		case R.id.mod1_1:
			Intent intent1 = new Intent(this, Act_Ex1_m4.class);
			startActivityForResult(intent1, 1000);
			overridePendingTransition(android.R.anim.fade_in,
					android.R.anim.fade_out);
			finish();
			break;
		case R.id.mod1_2:
			//			Intent intent2 = new Intent(this, Act_Page1_m5.class);
			//			startActivityForResult(intent2, 1000);
			//			overridePendingTransition(android.R.anim.fade_in,
			//					android.R.anim.fade_out);
			//			finish();
			break;
		case R.id.exit:
			//			Intent int_exit = new Intent(this, IntPrincipal.class);
			//			startActivityForResult(int_exit, 1000);
			//			overridePendingTransition(android.R.anim.fade_in,
			//					android.R.anim.fade_out);
			finish();
			break;
		case R.id.home:
			Intent int_home = new Intent(this, Act_Modules.class);
			startActivityForResult(int_home, 1000);
			overridePendingTransition(android.R.anim.fade_in,
					android.R.anim.fade_out);
			finish();
			break;

		default:
			break;
		}
	}

	@Override
	public void onBackPressed() {
		// Do Here what ever you want do on back press;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////	
	public void onWindowFocusChanged(boolean hasFocus) {
		if (!scalingComplete) // only do this once
		{
			scaleContents(findViewById(R.id.contents), findViewById(R.id.container));
			scalingComplete = true;
		}
		super.onWindowFocusChanged(hasFocus);
	}

	/** Called when the views have been created. We override this in order to scale
	 *  the UI, which we can't do before this. 
	 */
	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
		View view = super.onCreateView(name, context, attrs);
		return view;
	}

	private void scaleContents(View rootView, View container)
	{
		float xScale = (float)container.getWidth() / rootView.getWidth();
		float yScale = (float)container.getHeight() / rootView.getHeight();
		float scale = Math.min(xScale, yScale);
		scaleViewAndChildren(rootView, scale);
	}
	public static void scaleViewAndChildren(View root, float scale)
	{
		ViewGroup.LayoutParams layoutParams = root.getLayoutParams();

		// Scale the view itself
		if (layoutParams.width != ViewGroup.LayoutParams.FILL_PARENT && 
				layoutParams.width != ViewGroup.LayoutParams.WRAP_CONTENT)
		{
			layoutParams.width *= scale;
		}
		if (layoutParams.height != ViewGroup.LayoutParams.FILL_PARENT && 
				layoutParams.height != ViewGroup.LayoutParams.WRAP_CONTENT)
		{
			layoutParams.height *= scale;
		}

		if (layoutParams instanceof ViewGroup.MarginLayoutParams)
		{
			ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams)layoutParams;
			marginParams.leftMargin *= scale;
			marginParams.rightMargin *= scale;
			marginParams.topMargin *= scale;
			marginParams.bottomMargin *= scale;
		}


		root.setLayoutParams(layoutParams);


		root.setPadding(
				(int)(root.getPaddingLeft() * scale), 
				(int)(root.getPaddingTop() * scale), 
				(int)(root.getPaddingRight() * scale), 
				(int)(root.getPaddingBottom() * scale));


		if (root instanceof TextView)
		{
			TextView textView = (TextView)root; 
			textView.setTextSize(textView.getTextSize() * scale);
		}

		if (root instanceof ViewGroup)
		{
			ViewGroup groupView = (ViewGroup)root;
			for (int cnt = 0; cnt < groupView.getChildCount(); ++cnt)
				scaleViewAndChildren(groupView.getChildAt(cnt), scale);
		}
	} 

}
